const express = require('express');
const app = express();
const port = 8000;
const mongoose = require('mongoose');
const userRouter = require('./app/routes/user.router');
const postRouter = require('./app/routes/post.router');
const commentRouter = require('./app/routes/comment.router');
const albumRouter = require('./app/routes/album.router');
const photoRouter = require('./app/routes/photo.router');
const todoRouter = require('./app/routes/todo.router');

//middleware
app.use(express.json());

app.use((req, res, next) => {
    const currentDate = new Date();
    console.log(`Current Date and Time: ${currentDate}`);
    next();
});

// Middleware để in URL của request ra console
app.use((req, res, next) => {
    console.log(`Requested method: ${req.method}`);
    next();
});

// khai báo kết nối mongoDB qua mongoose
mongoose.connect("mongodb://127.0.0.1:27017/zigvy-interview-nodejs")
    .then(() => {
        console.log("MongoDB connected");
    })
    .catch((err) => {
        console.log(err);
    });

// Routes
app.get('/', (req, res) => {
    res.send('Welcome to Devcamp Lucky Dice Nodejs!');
});

app.use('/users', userRouter);
app.use('/posts', postRouter);
app.use('/comments', commentRouter);
app.use('/albums', albumRouter);
app.use('/photos', photoRouter);
app.use('/todos', todoRouter);

app.listen(port, () => {
    console.log(`Server is running on http://localhost:${port}`);
})