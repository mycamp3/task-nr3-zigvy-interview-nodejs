const express = require('express');
const router = express.Router();
const {
    createAlbum,
    getAllAlbums,
    getAlbumById,
    updateAlbumById,
    deleteAlbumById,
    getAlbumsOfUser
} = require('../controllers/album.controller');

// POST /albums - Create a new album
router.post('/', createAlbum);

// GET /albums - Get all albums
router.get('/', getAllAlbums);

// GET /albums/:albumId - Get an album by ID
router.get('/:albumId', getAlbumById);

// PUT /albums/:albumId - Update an album by ID
router.put('/:albumId', updateAlbumById);

// DELETE /albums/:albumId - Delete an album by ID
router.delete('/:albumId', deleteAlbumById);

// GET /users/:userId/albums - Get albums of a user
router.get('/users/:userId', getAlbumsOfUser);

module.exports = router;
