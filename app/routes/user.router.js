const express = require('express');
const router = express.Router();
const { createUser, getAllUser, getUserById, updateUserById, deleteUserById } = require('../controllers/user.controller');

// POST /users - Create a new user
router.post('/', createUser);

// GET /users - Get all users
router.get('/', getAllUser);

// GET /users/:userId - Get a user by ID
router.get('/:userId', getUserById);

// PUT /users/:userId - Update a user by ID
router.put('/:userId', updateUserById);

// DELETE /users/:userId - Delete a user by ID
router.delete('/:userId', deleteUserById);

module.exports = router;