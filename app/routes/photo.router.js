const express = require('express');
const router = express.Router();
const {
    createPhoto,
    getAllPhotos,
    getPhotoById,
    updatePhotoById,
    deletePhotoById,
    getPhotosOfAlbum
} = require('../controllers/photo.controller');

// POST /photos - Create a new photo
router.post('/', createPhoto);

// GET /photos - Get all photos, optionally filtered by albumId
router.get('/', getAllPhotos);

// GET /photos/:photoId - Get a photo by ID
router.get('/:photoId', getPhotoById);

// PUT /photos/:photoId - Update a photo by ID
router.put('/:photoId', updatePhotoById);

// DELETE /photos/:photoId - Delete a photo by ID
router.delete('/:photoId', deletePhotoById);

// GET /albums/:albumId/photos - Get all photos of an album
router.get('/albums/:albumId', getPhotosOfAlbum);

module.exports = router;
