const express = require('express');
const router = express.Router();
const {
    createTodo,
    getAllTodos,
    getTodoById,
    updateTodoById,
    deleteTodoById,
    getTodosOfUser
} = require('../controllers/todo.controller');

// POST /todos - Create a new todo
router.post('/', createTodo);

// GET /todos - Get all todos (optionally filtered by userId)
router.get('/', getAllTodos);

// GET /todos/:todoId - Get a todo by ID
router.get('/:todoId', getTodoById);

// PUT /todos/:todoId - Update a todo by ID
router.put('/:todoId', updateTodoById);

// DELETE /todos/:todoId - Delete a todo by ID
router.delete('/:todoId', deleteTodoById);

// GET /users/:userId/todos - Get all todos of a user
router.get('/users/:userId', getTodosOfUser);

module.exports = router;
