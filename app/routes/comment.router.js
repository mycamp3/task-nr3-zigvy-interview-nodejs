const express = require('express');
const router = express.Router();
const {
    createComment,
    getAllComments,
    getCommentsOfPost,
    getCommentById,
    updateCommentById,
    deleteCommentById
} = require('../controllers/comment.controller');

// POST /comments - Create a new comment
router.post('/', createComment);

// GET /comments - Get all comments
router.get('/', getAllComments);

// GET /comments/:commentId - Get a comment by ID
router.get('/:commentId', getCommentById);

// PUT /comments/:commentId - Update a comment by ID
router.put('/:commentId', updateCommentById);

// DELETE /comments/:commentId - Delete a comment by ID
router.delete('/:commentId', deleteCommentById);

// GET /posts/:postId/comments - Get comments of a post by postId
router.get('/posts/:postId', getCommentsOfPost);
module.exports = router;
