const express = require('express');
const router = express.Router();
const {
    createPost,
    getAllPost,
    getPostById,
    updatePostById,
    deletePostById,
    getPostsOfUser
} = require('../controllers/post.controller');

// POST /posts - Create a new post
router.post('/', createPost);

// GET /posts - Get all posts (with optional query parameter for userId)
router.get('/', getAllPost);

// GET /posts/:postId - Get a post by ID
router.get('/:postId', getPostById);

// PUT /posts/:postId - Update a post by ID
router.put('/:postId', updatePostById);

// DELETE /posts/:postId - Delete a post by ID
router.delete('/:postId', deletePostById);

// GET /users/:userId/posts - Get all posts of a user
router.get('/users/:userId', getPostsOfUser);

module.exports = router;
