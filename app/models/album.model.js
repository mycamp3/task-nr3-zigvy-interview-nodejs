const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const albumSchema = new Schema({
    userId: {
        type: mongoose.Types.ObjectId,
        ref: "user"
    },
    title: {
        type: String,
        require: true
    }
})
module.exports = mongoose.model("Album", albumSchema)