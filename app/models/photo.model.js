const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const photoSchema = new Schema({
    albumId: {
        type: mongoose.Types.ObjectId,
        ref: 'album',
        required: true
    },
    title: {
        type: String,
        required: true
    },
    url: {
        type: String,
        required: true,
        validate: {
            validator: function(v) {
                return /^https?:\/\/[^\s$.?#].[^\s]*$/gm.test(v);
            },
            message: props => `${props.value} is not a valid URL`
        }
    },
    thumbnailUrl: {
        type: String,
        required: true,
        validate: {
            validator: function(v) {
                return /^https?:\/\/[^\s$.?#].[^\s]*$/gm.test(v);
            },
            message: props => `${props.value} is not a valid URL`
        }
    }
});

module.exports = mongoose.model('Photo', photoSchema);
