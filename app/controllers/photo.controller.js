const mongoose = require('mongoose');
const Photo = require('../models/photo.model');
const Album = require('../models/album.model');

// Validate ObjectId
const isValidObjectId = (id) => mongoose.Types.ObjectId.isValid(id);

// Create a new photo
const createPhoto = async (req, res) => {
    try {
        const { albumId, title, url, thumbnailUrl } = req.body;

        if (!isValidObjectId(albumId)) {
            return res.status(400).json({ error: 'Invalid albumId' });
        }

        const album = await Album.findById(albumId);
        if (!album) {
            return res.status(404).json({ error: 'Album not found' });
        }

        const newPhoto = new Photo({ albumId, title, url, thumbnailUrl });
        await newPhoto.save();

        res.status(201).json(newPhoto);
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
};

// Get all photos, optionally filtered by albumId
const getAllPhotos = async (req, res) => {
    try {
        const { albumId } = req.query;
        let photos;

        if (albumId) {
            if (!isValidObjectId(albumId)) {
                return res.status(400).json({ error: 'Invalid albumId' });
            }

            const album = await Album.findById(albumId);
            if (!album) {
                return res.status(404).json({ error: 'Album not found' });
            }

            photos = await Photo.find({ albumId });
        } else {
            photos = await Photo.find();
        }

        res.status(200).json(photos);
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
};

// Get photo by ID
const getPhotoById = async (req, res) => {
    try {
        const { photoId } = req.params;

        if (!isValidObjectId(photoId)) {
            return res.status(400).json({ error: 'Invalid photoId' });
        }

        const photo = await Photo.findById(photoId);
        if (!photo) {
            return res.status(404).json({ error: 'Photo not found' });
        }

        res.status(200).json(photo);
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
};

// Update photo by ID
const updatePhotoById = async (req, res) => {
    try {
        const { photoId } = req.params;

        if (!isValidObjectId(photoId)) {
            return res.status(400).json({ error: 'Invalid photoId' });
        }

        const updatedPhoto = await Photo.findByIdAndUpdate(photoId, req.body, { new: true, runValidators: true });
        if (!updatedPhoto) {
            return res.status(404).json({ error: 'Photo not found' });
        }

        res.status(200).json(updatedPhoto);
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
};

// Delete photo by ID
const deletePhotoById = async (req, res) => {
    try {
        const { photoId } = req.params;

        if (!isValidObjectId(photoId)) {
            return res.status(400).json({ error: 'Invalid photoId' });
        }

        const deletedPhoto = await Photo.findByIdAndDelete(photoId);
        if (!deletedPhoto) {
            return res.status(404).json({ error: 'Photo not found' });
        }

        res.status(200).json({ message: 'Photo deleted successfully' });
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
};

// Get photos of an album
const getPhotosOfAlbum = async (req, res) => {
    try {
        const { albumId } = req.params;

        if (!isValidObjectId(albumId)) {
            return res.status(400).json({ error: 'Invalid albumId' });
        }

        const album = await Album.findById(albumId);
        if (!album) {
            return res.status(404).json({ error: 'Album not found' });
        }

        const photos = await Photo.find({ albumId });
        res.status(200).json(photos);
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
};

module.exports = {
    createPhoto,
    getAllPhotos,
    getPhotoById,
    updatePhotoById,
    deletePhotoById,
    getPhotosOfAlbum
};
