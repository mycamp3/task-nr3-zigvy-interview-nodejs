const mongoose = require('mongoose');
const Todo = require('../models/todo.model');
const User = require('../models/user.model');

// Validate ObjectId
const isValidObjectId = (id) => mongoose.Types.ObjectId.isValid(id);

// Create a new todo
const createTodo = async (req, res) => {
    try {
        const { userId, title, completed } = req.body;

        if (!isValidObjectId(userId)) {
            return res.status(400).json({ error: 'Invalid userId' });
        }

        const user = await User.findById(userId);
        if (!user) {
            return res.status(404).json({ error: 'User not found' });
        }

        const newTodo = new Todo({
            userId,
            title,
            completed: completed || false
        });

        await newTodo.save();
        res.status(201).json(newTodo);
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
};

// Get all todos, optionally filtered by userId
const getAllTodos = async (req, res) => {
    try {
        const { userId } = req.query;
        let todos;

        if (userId) {
            if (!isValidObjectId(userId)) {
                return res.status(400).json({ error: 'Invalid userId' });
            }

            const user = await User.findById(userId);
            if (!user) {
                return res.status(404).json({ error: 'User not found' });
            }

            todos = await Todo.find({ userId });
        } else {
            todos = await Todo.find();
        }

        res.status(200).json(todos);
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
};

// Get todo by ID
const getTodoById = async (req, res) => {
    try {
        const { todoId } = req.params;

        if (!isValidObjectId(todoId)) {
            return res.status(400).json({ error: 'Invalid todoId' });
        }

        const todo = await Todo.findById(todoId);
        if (!todo) {
            return res.status(404).json({ error: 'Todo not found' });
        }

        res.status(200).json(todo);
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
};

// Update todo by ID
const updateTodoById = async (req, res) => {
    try {
        const { todoId } = req.params;

        if (!isValidObjectId(todoId)) {
            return res.status(400).json({ error: 'Invalid todoId' });
        }

        const updatedTodo = await Todo.findByIdAndUpdate(todoId, req.body, {
            new: true,
            runValidators: true
        });

        if (!updatedTodo) {
            return res.status(404).json({ error: 'Todo not found' });
        }

        res.status(200).json(updatedTodo);
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
};

// Delete todo by ID
const deleteTodoById = async (req, res) => {
    try {
        const { todoId } = req.params;

        if (!isValidObjectId(todoId)) {
            return res.status(400).json({ error: 'Invalid todoId' });
        }

        const deletedTodo = await Todo.findByIdAndDelete(todoId);

        if (!deletedTodo) {
            return res.status(404).json({ error: 'Todo not found' });
        }

        res.status(200).json({ message: 'Todo deleted successfully' });
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
};

// Get todos of a user
const getTodosOfUser = async (req, res) => {
    try {
        const { userId } = req.params;

        if (!isValidObjectId(userId)) {
            return res.status(400).json({ error: 'Invalid userId' });
        }

        const user = await User.findById(userId);
        if (!user) {
            return res.status(404).json({ error: 'User not found' });
        }

        const todos = await Todo.find({ userId });
        res.status(200).json(todos);
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
};

module.exports = {
    createTodo,
    getAllTodos,
    getTodoById,
    updateTodoById,
    deleteTodoById,
    getTodosOfUser
};
