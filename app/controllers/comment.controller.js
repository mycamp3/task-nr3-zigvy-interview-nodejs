const Comment = require('../models/comment.model');
const mongoose = require('mongoose');
const Post = require('../models/post.model')
// Controller function to create a new comment
const createComment = async (req, res) => {
    try {
        const { postId, name, email, body } = req.body;
        // Validate required fields
        if (!postId || !name || !email || !body) {
            return res.status(400).json({ error: 'All fields are required' });
        }

        // Validate postId
        if (!mongoose.Types.ObjectId.isValid(postId)) {
            return res.status(400).json({ error: 'Invalid postId' });
        }
        const newComment = new Comment({ postId, name, email, body });
        const savedComment = await newComment.save();
        res.status(201).json(savedComment);
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
};

const getAllComments = async (req, res) => {
    try {
        let query = {}; // Initialize empty filter object

        // Check if query parameter 'postId' exists
        if (req.query.postId) {
            query.postId = req.query.postId; // Set postId filter if provided in query
            
            if (!mongoose.Types.ObjectId.isValid(query.postId)) {
                return res.status(400).json({ error: 'Invalid postId' });
            }

            const user = await Post.findById(query.postId);
            if (!user) {
                return res.status(404).json({ error: 'Post not found' });
            }

        }
        
        // Fetch comments with optional filter
        const comments = await Comment.find(query);

        res.status(200).json(comments);
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
};

// Controller function to get a comment by ID
const getCommentById = async (req, res) => {
    try {
        const comment = await Comment.findById(req.params.commentId);
        if (!comment) {
            return res.status(404).json({ error: 'Comment not found' });
        }
        res.status(200).json(comment);
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
};

// Controller function to get comments of a post by postId
const getCommentsOfPost = async (req, res) => {
    try {
        const postId = req.params.postId;

        // Validate postId
        if (!mongoose.Types.ObjectId.isValid(postId)) {
            return res.status(400).json({ error: 'Invalid postId' });
        }

        // Fetch comments with the specified postId
        const comments = await Comment.find({ postId });

        res.status(200).json(comments);
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
};

// Controller function to update a comment by ID
const updateCommentById = async (req, res) => {
    try {
        const { name, email, body } = req.body;
        const updatedFields = {};
        
        if (name) updatedFields.name = name;
        if (email) updatedFields.email = email;
        if (body) updatedFields.body = body;

        const updatedComment = await Comment.findByIdAndUpdate(
            req.params.commentId,
            updatedFields,
            { new: true } // To return the updated document
        );

        if (!updatedComment) {
            return res.status(404).json({ error: 'Comment not found' });
        }

        res.status(200).json(updatedComment);
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
};

// Controller function to delete a comment by ID
const deleteCommentById = async (req, res) => {
    try {
        const deletedComment = await Comment.findByIdAndDelete(req.params.commentId);
        if (!deletedComment) {
            return res.status(404).json({ error: 'Comment not found' });
        }
        res.status(200).json({ message: 'Comment deleted successfully' });
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
};

module.exports = {
    createComment,
    getAllComments,
    getCommentsOfPost,
    getCommentById,
    updateCommentById,
    deleteCommentById
};
