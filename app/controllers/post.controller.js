const mongoose = require('mongoose');
const Post = require('../models/post.model');

// Validate ObjectId format
const isValidObjectId = (id) => mongoose.Types.ObjectId.isValid(id);

// Create a new post
const createPost = async (req, res) => {
    const { userId, title, body } = req.body;
    if (!userId || !title || !body) {
        return res.status(400).json({ error: "All fields are required" });
    }

    try {
        const post = new Post({
            userId,
            title,
            body
        });
        const result = await post.save();
        res.status(201).json(result);
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
};

// Get all posts or filter by userId
const getAllPost = async (req, res) => {
    const { userId } = req.query;
    try {
        let query = {};
        if (userId) {
            query.userId = userId; // Filter by userId if provided
        }
        const posts = await Post.find(query);
        res.status(200).json(posts);
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
};

// Get a post by ID
const getPostById = async (req, res) => {
    const { postId } = req.params;

    if (!isValidObjectId(postId)) {
        return res.status(400).json({ error: "Invalid postId format" });
    }

    try {
        const post = await Post.findById(postId);
        if (post) {
            res.status(200).json(post);
        } else {
            res.status(404).json({ message: "Post not found" });
        }
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
};

// Update a post by ID
const updatePostById = async (req, res) => {
    const { postId } = req.params;
    const { title, body } = req.body;

    if (!isValidObjectId(postId)) {
        return res.status(400).json({ error: "Invalid postId format" });
    }

    if (!title && !body) {
        return res.status(400).json({ error: "At least one field (title or body) is required" });
    }

    try {
        const updateData = {};
        if (title) updateData.title = title;
        if (body) updateData.body = body;

        const post = await Post.findByIdAndUpdate(postId, updateData, { new: true });
        if (post) {
            res.status(200).json(post);
        } else {
            res.status(404).json({ message: "Post not found" });
        }
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
};

// Delete a post by ID
const deletePostById = async (req, res) => {
    const { postId } = req.params;

    if (!isValidObjectId(postId)) {
        return res.status(400).json({ error: "Invalid postId format" });
    }

    try {
        const post = await Post.findByIdAndDelete(postId);
        if (post) {
            res.status(200).json({ message: "Post deleted successfully" });
        } else {
            res.status(404).json({ message: "Post not found" });
        }
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
};

// Get all posts of a user
const getPostsOfUser = async (req, res) => {
    try {
        const userId = req.params.userId;

        // Validate postId
        if (!mongoose.Types.ObjectId.isValid(userId)) {
            return res.status(400).json({ error: 'Invalid userId' });
        }
        const posts = await Post.find({ userId });
        res.status(200).json(posts);
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
};

module.exports = {
    createPost,
    getAllPost,
    getPostById,
    updatePostById,
    deletePostById,
    getPostsOfUser
};
