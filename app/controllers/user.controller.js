const mongoose = require('mongoose');
const User = require('../models/user.model');

// Validate ObjectId format
const isValidObjectId = (id) => mongoose.Types.ObjectId.isValid(id);

// Helper function to validate user data
const validateUserData = (data) => {
    const { name, username, address, phone, website, company } = data;
    if (!name || !username || !address || !phone || !website || !company) {
        return 'All fields are required';
    }
    if (!address.street || !address.suite || !address.city || !address.zipcode || !address.geo || !address.geo.lat || !address.geo.lng) {
        return 'All address fields are required';
    }
    if (!company.name || !company.catchPhrase || !company.bs) {
        return 'All company fields are required';
    }
    // Add more custom validation logic as needed
    return null;
};

// Create a new user
const createUser = async (req, res) => {
    const validationError = validateUserData(req.body);
    if (validationError) {
        return res.status(400).json({ error: validationError });
    }

    try {
        const user = new User({
            ...req.body,
            _id: new mongoose.Types.ObjectId()
        });
        const result = await user.save();
        res.status(201).json(result);
    } catch (error) {
        if (error.code === 11000) {
            // Duplicate username
            return res.status(400).json({ error: 'Username already exists' });
        }
        res.status(500).json({ error: error.message });
    }
};

// Get all users
const getAllUser = async (req, res) => {
    try {
        const users = await User.find();
        res.status(200).json(users);
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
};

// Get a user by ID
const getUserById = async (req, res) => {
    const { userId } = req.params;

    if (!isValidObjectId(userId)) {
        return res.status(400).json({ error: "Invalid userId format" });
    }

    try {
        const user = await User.findById(userId);
        if (user) {
            res.status(200).json(user);
        } else {
            res.status(404).json({ message: "User not found" });
        }
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
};

// Update a user by ID
const updateUserById = async (req, res) => {
    const { userId } = req.params;
    const { name, username, address, phone, website, company } = req.body;

    if (!isValidObjectId(userId)) {
        return res.status(400).json({ error: "Invalid userId format" });
    }

    try {
        const updateData = {};
        if (name) updateData.name = name;
        if (username) updateData.username = username;
        if (address) updateData.address = address;
        if (phone) updateData.phone = phone;
        if (website) updateData.website = website;
        if (company) updateData.company = company;

        const user = await User.findByIdAndUpdate(userId, updateData, { new: true });
        if (user) {
            res.status(200).json(user);
        } else {
            res.status(404).json({ message: "User not found" });
        }
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
};

// Delete a user by ID
const deleteUserById = async (req, res) => {
    const { userId } = req.params;

    if (!isValidObjectId(userId)) {
        return res.status(400).json({ error: "Invalid userId format" });
    }

    try {
        const user = await User.findByIdAndDelete(userId);
        if (user) {
            res.status(200).json({ message: "User deleted successfully" });
        } else {
            res.status(404).json({ message: "User not found" });
        }
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
};

module.exports = {
    createUser,
    getAllUser,
    getUserById,
    updateUserById,
    deleteUserById
};

