const mongoose = require('mongoose');
const Album = require('../models/album.model');
const User = require('../models/user.model');  // Add this line to import the User model

// Validate ObjectId
const isValidObjectId = (id) => mongoose.Types.ObjectId.isValid(id);

// Create a new album
const createAlbum = async (req, res) => {
    try {
        const { userId, title } = req.body;

        if (!isValidObjectId(userId)) {
            return res.status(400).json({
                error: 'Invalid userId'
            });
        }

        const user = await User.findById(userId);
        if (!user) {
            return res.status(404).json({
                error: 'User not found'
            });
        }

        const newAlbum = new Album({ userId, title });
        await newAlbum.save();

        res.status(201).json(newAlbum);
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
};

// Get all albums
const getAllAlbums = async (req, res) => {
    try {
        const { userId } = req.query;
        let albums;

        if (userId) {
            if (!isValidObjectId(userId)) {
                return res.status(400).json({ error: 'Invalid userId' });
            }

            const user = await User.findById(userId);
            if (!user) {
                return res.status(404).json({ error: 'User not found' });
            }

            albums = await Album.find({ userId });
        } else {
            albums = await Album.find();
        }
        res.status(200).json(albums);
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
};

// Get album by ID
const getAlbumById = async (req, res) => {
    try {
        const { albumId } = req.params;

        if (!isValidObjectId(albumId)) {
            return res.status(400).json({ error: 'Invalid albumId' });
        }

        const album = await Album.findById(albumId);
        if (!album) {
            return res.status(404).json({ error: 'Album not found' });
        }

        res.status(200).json(album);
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
};

// Update album by ID
const updateAlbumById = async (req, res) => {
    try {
        const { albumId } = req.params;

        if (!isValidObjectId(albumId)) {
            return res.status(400).json({ error: 'Invalid albumId' });
        }

        const updatedAlbum = await Album.findByIdAndUpdate(albumId, req.body, { new: true, runValidators: true });
        if (!updatedAlbum) {
            return res.status(404).json({ error: 'Album not found' });
        }

        res.status(200).json(updatedAlbum);
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
};

// Delete album by ID
const deleteAlbumById = async (req, res) => {
    try {
        const { albumId } = req.params;

        if (!isValidObjectId(albumId)) {
            return res.status(400).json({ error: 'Invalid albumId' });
        }

        const deletedAlbum = await Album.findByIdAndDelete(albumId);
        if (!deletedAlbum) {
            return res.status(404).json({ error: 'Album not found' });
        }

        res.status(200).json({ message: 'Album deleted successfully' });
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
};

// Get albums of a user
const getAlbumsOfUser = async (req, res) => {
    try {
        const { userId } = req.params;

        if (!isValidObjectId(userId)) {
            return res.status(400).json({ error: 'Invalid userId' });
        }

        const user = await User.findById(userId);
        if (!user) {
            return res.status(404).json({ error: 'User not found' });
        }

        const albums = await Album.find({ userId });
        res.status(200).json(albums);
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
};

module.exports = {
    createAlbum,
    getAllAlbums,
    getAlbumById,
    updateAlbumById,
    deleteAlbumById,
    getAlbumsOfUser
};
